namespace NickWardOnline.Migrations.Migrations
{
	using Microsoft.AspNet.Identity;
	using Microsoft.AspNet.Identity.EntityFramework;
	using NickWardOnline.Models.CustomModels;
	using System.Data.Entity.Migrations;
	using System.Linq;

	internal sealed class Configuration : DbMigrationsConfiguration<Models.Context.CmsContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}

		protected override void Seed(Models.Context.CmsContext context)
		{
			if(!context.Roles.Any(r => r.Name == "admin"))
			{
				var store = new RoleStore<IdentityRole>(context);
				var manager = new RoleManager<IdentityRole>(store);
				var role = new IdentityRole { Name = "admin" };

				manager.Create(role);
			}

			if (!context.Roles.Any(r => r.Name == "editor"))
			{
				var store = new RoleStore<IdentityRole>(context);
				var manager = new RoleManager<IdentityRole>(store);
				var role = new IdentityRole { Name = "editor" };

				manager.Create(role);
			}

			if (!context.Roles.Any(r => r.Name == "author"))
			{
				var store = new RoleStore<IdentityRole>(context);
				var manager = new RoleManager<IdentityRole>(store);
				var role = new IdentityRole { Name = "author" };

				manager.Create(role);
			}

			if (!context.Users.Any(u => u.UserName == "admin"))
			{
				var store = new UserStore<CmsUser>(context);
				var manager = new UserManager<CmsUser>(store);
				var user = new CmsUser
				{
					UserName = "admin",
					DisplayName = "Administrator",
					Email = "admin@cms.com"
				};

				manager.Create(user, "Passw0rd1234");
				manager.AddToRole(user.Id, "admin");
			}

			if (!context.Users.Any(u => u.UserName == "author"))
			{
				var store = new UserStore<CmsUser>(context);
				var manager = new UserManager<CmsUser>(store);
				var user = new CmsUser
				{
					UserName = "author",
					DisplayName = "Author",
					Email = "author@cms.com"
				};

				manager.Create(user, "Passw0rd1234");
				manager.AddToRole(user.Id, "author");
			}

			if (!context.Users.Any(u => u.UserName == "editor"))
			{
				var store = new UserStore<CmsUser>(context);
				var manager = new UserManager<CmsUser>(store);
				var user = new CmsUser
				{
					UserName = "editor",
					DisplayName = "Editor",
					Email = "editor@cms.com"
				};

				manager.Create(user, "Passw0rd1234");
				manager.AddToRole(user.Id, "editor");
			}
		}
	}
}
