﻿CREATE TABLE [dbo].[Post] (
    [Id]           NVARCHAR (128) NOT NULL,
    [Title]        NVARCHAR (MAX) NOT NULL,
    [Content]      NVARCHAR (MAX) NOT NULL,
    [ImagePath]    VARCHAR (MAX)  NULL,
    [Created]      DATETIME       NOT NULL,
    [Published]    DATETIME       NULL,
    [CombinedTags] NVARCHAR (MAX) NULL,
    [AuthorId]     NVARCHAR (128) NULL,
    CONSTRAINT [PK_dbo.Posts] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Posts_dbo.AspNetUsers_AuthorId] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);

