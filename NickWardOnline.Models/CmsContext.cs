﻿namespace NickWardOnline.Models.Context
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models.CustomModels;
	using NickWardOnline.Models.DomainModels;
	using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
	using System.Data.Entity.ModelConfiguration.Conventions;

	/// <summary>
	/// Database context class
	/// </summary>
	public class CmsContext : IdentityDbContext<CmsUser>
    {
        public CmsContext()
            : base("Name=CmsConnectionString")
		{
			Database.SetInitializer<CmsContext>(new CreateDatabaseIfNotExists<CmsContext>());
		}

		public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.Entity<Post>()
                .HasKey(e => e.Id)
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<Post>()
                .HasRequired(e => e.Author);
        }
    }
}
