﻿namespace NickWardOnline.Models.DomainModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// Post domain model
    /// </summary>
    public class Post
    {
        private IList<string> _tags = new List<string>();

        [Key]
        public string Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public string ImagePath { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Published { get; set; }

        public IList<string> Tags
        {
            get { return _tags; }
            set { _tags = value; }
        }

        public string CombinedTags
        {
            get { return string.Join(",", _tags); }
            set
            {
                _tags = value.Split(',').Select(s => s.Trim()).ToList();
            }
        }

        public string AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public virtual NickWardOnline.Models.CustomModels.CmsUser Author { get; set; }
    }
}
