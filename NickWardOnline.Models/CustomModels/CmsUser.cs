﻿namespace NickWardOnline.Models.CustomModels
{
    using Microsoft.AspNet.Identity.EntityFramework;

    /// <summary>
    /// CmsUser model
    /// </summary>
    public class CmsUser : IdentityUser
    {
        public string DisplayName { get; set; }
    }
}
