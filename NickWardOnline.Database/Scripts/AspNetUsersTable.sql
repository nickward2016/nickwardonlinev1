﻿/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 28/10/2017 18:22:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NULL,
	[TwoFactorEnabled] [bit] NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NULL,
	[AccessFailedCount] [int] NULL,
	[UserName] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

INSERT [dbo].[AspNetUsers] 
([Id], 
[DisplayName],
[UserName],
[Email], 
[EmailConfirmed], 
[PasswordHash],
[SecurityStamp],
[PhoneNumberConfirmed],
[TwoFactorEnabled],
[LockoutEnabled],
[AccessFailedCount])
VALUES 
(N'38957da4-007c-4a82-9a0a-045a2f525698',
N'Administrator',
N'admin',
N'admin@nickwardonline.co.uk', 
1,
N'ADO5LK7M6BFXs9FN53v6wOJuQPRCVPOtewASNAUc22pkQf69uw8dHVe6h8RkCliJ1g==',
N'03adae53-88d5-4a6d-9928-889edd30f253',
0,
0,
0,
0)

GO

INSERT [dbo].[AspNetUsers] 
([Id], 
[DisplayName],
[UserName],
[Email], 
[EmailConfirmed], 
[PasswordHash],
[SecurityStamp],
[PhoneNumberConfirmed],
[TwoFactorEnabled],
[LockoutEnabled],
[AccessFailedCount])
VALUES 
(N'4436cde0-36eb-4d4a-a3b4-b6170a96e7d9',
N'Editor',
N'editor',
N'editor@nickwardonline.co.uk', 
1,
N'ADO5LK7M6BFXs9FN53v6wOJuQPRCVPOtewASNAUc22pkQf69uw8dHVe6h8RkCliJ1g==',
N'bea45e73-52b5-4d09-bf78-35a2fbab372a',
0,
0,
0,
0)

GO

INSERT [dbo].[AspNetUsers] 
([Id], 
[DisplayName],
[UserName],
[Email], 
[EmailConfirmed], 
[PasswordHash],
[SecurityStamp],
[PhoneNumberConfirmed],
[TwoFactorEnabled],
[LockoutEnabled],
[AccessFailedCount])
VALUES 
(N'da49a2dd-0912-4ade-94d3-7b7516c6e00b',
N'Author',
N'author',
N'author@nickwardonline.co.uk', 
1,
N'ADO5LK7M6BFXs9FN53v6wOJuQPRCVPOtewASNAUc22pkQf69uw8dHVe6h8RkCliJ1g==',
N'898efce9-37b0-4870-a110-beb43814fee6',
0,
0,
0,
0)

GO