﻿/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 28/10/2017 18:15:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT [dbo].[AspNetRoles] 
([Id], 
[Name])
VALUES 
(N'ffe2e2ee-c815-448a-a3d8-99974683307b',
N'admin')

GO

INSERT [dbo].[AspNetRoles] 
([Id], 
[Name])
VALUES 
(N'589cc570-b4a9-4628-afd1-1796bc85c7d0',
N'author')

GO

INSERT [dbo].[AspNetRoles] 
([Id], 
[Name])
VALUES 
(N'167e97fa-80d3-4942-aac6-27876ddee3f4',
N'editor')