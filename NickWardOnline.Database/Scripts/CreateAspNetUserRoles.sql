﻿/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 28/10/2017 18:13:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO

INSERT [dbo].[AspNetUserRoles] 
([UserId], 
[RoleId])
VALUES 
(N'38957da4-007c-4a82-9a0a-045a2f525698',
N'ffe2e2ee-c815-448a-a3d8-99974683307b')

GO

INSERT [dbo].[AspNetUserRoles] 
([UserId], 
[RoleId])
VALUES 
(N'da49a2dd-0912-4ade-94d3-7b7516c6e00b',
N'589cc570-b4a9-4628-afd1-1796bc85c7d0')

INSERT [dbo].[AspNetUserRoles] 
([UserId], 
[RoleId])
VALUES 
(N'4436cde0-36eb-4d4a-a3b4-b6170a96e7d9',
N'167e97fa-80d3-4942-aac6-27876ddee3f4')

GO