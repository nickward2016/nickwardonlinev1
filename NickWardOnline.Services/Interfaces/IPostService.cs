﻿namespace NickWardOnline.Services
{
	using NickWardOnline.Models.DomainModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;

    /// <summary>
    /// IPostService interface
    /// </summary>
    public interface IPostService
    {
        int CountPublishedPosts();
        Post Get(string id);
        Task<IEnumerable<Post>> GetAllAsync();
        Task<IEnumerable<Post>> GetByAuthorAsync(string authorId);
        bool Create(HttpPostedFileBase file, Post model);
        bool Edit(HttpPostedFileBase file, string id, Post updatedItem);
        bool Delete(string id);
        Task<IEnumerable<Post>> GetByTagAsync(string tagId);
        Task<IEnumerable<Post>> GetPublishedPostsAsync();
        Task<IEnumerable<Post>> GetPageAsync(int pageNumber, int pageSize);
		Task<IEnumerable<Post>> GetPostsByAuthorAsync(string authorId);

	}
}
