﻿namespace NickWardOnline.Services
{
	using Microsoft.AspNet.Identity.EntityFramework;
	using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// IRoleService interface
    /// </summary>
    public interface IRoleService
    {
        bool CreateAsync(IdentityRole role);
        Task<IdentityRole> GetRoleByNameAsync(string name);
        Task<IEnumerable<IdentityRole>> GetAllRolesAsync();
    }
}
