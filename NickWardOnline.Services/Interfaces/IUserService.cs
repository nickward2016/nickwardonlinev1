﻿namespace NickWardOnline.Services
{
	using NickWardOnline.Models.CustomModels;
	using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;

    /// <summary>
    /// IUserService interface
    /// </summary>
    public interface IUserService : IDisposable
    {
        Task<CmsUser> GetUserByNameAsync(string username);
        Task<IEnumerable<CmsUser>> GetAllUsersAsync();
        bool CreateAsync(CmsUser user, string password);
        bool DeleteAsync(CmsUser user);
        bool UpdateAsync(CmsUser user);
        bool VerifyUserPassword(string hashedPassword, string providedPassword);
        string HashPassword(string password);
        bool AddUserToRoleAsync(CmsUser newUser, string role);
        Task<IEnumerable<string>> GetRolesForUserAsync(CmsUser user);
        bool RemoveUserFromRoleAsync(CmsUser user, params string[] roleNames);
        Task<CmsUser> GetLoginUserAsync(string userName, string password);
        Task<ClaimsIdentity> CreateIdentityAsync(CmsUser user);
    }
}
