﻿namespace NickWardOnline.Services
{
    using System.Collections.Generic;

    /// <summary>
    /// ITagService interface
    /// </summary>
    public interface ITagService
    {
        string Get(string tag);
        IEnumerable<string> GetAll();
        bool Delete(string tag);
        bool Edit(string existingTag, string newTag);
    }
}
