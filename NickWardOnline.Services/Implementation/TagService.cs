﻿namespace NickWardOnline.Services
{
	using NickWardOnline.Repositories;
	using System.Collections.Generic;

    /// <summary>
    /// TagService class
    /// </summary>
    public class TagService : ITagService
    {
        private readonly ITagRepository _tagRepository;

        public TagService()
            :this(new TagRepository()) { }

        public TagService(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }

        public bool Delete(string tag)
        {
            try
            {
                _tagRepository.Delete(tag);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Edit(string existingTag, string newTag)
        {
            try
            {
                _tagRepository.Edit(existingTag, newTag);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public string Get(string tag)
        {
            var tags = _tagRepository.Get(tag);

            return tags;
        }

        public IEnumerable<string> GetAll()
        {
            var tags = _tagRepository.GetAll();

            return tags;
        }
    }
}
