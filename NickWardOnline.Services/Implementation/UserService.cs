﻿namespace NickWardOnline.Services
{
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
	using NickWardOnline.Models.CustomModels;
	using NickWardOnline.Repositories;

	/// <summary>
	/// UserService class
	/// </summary>
	public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService()
            :this(new UserRepository()) { }

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool AddUserToRoleAsync(CmsUser newUser, string role)
        {
            try
            {
                _userRepository.AddUserToRoleAsync(newUser, role);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateAsync(CmsUser user, string password)
        {
            try
            {
                _userRepository.CreateAsync(user, password);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Task<ClaimsIdentity> CreateIdentityAsync(CmsUser user)
        {
            var identity = _userRepository.CreateIdentityAsync(user);

            return identity;
        }

        public bool DeleteAsync(CmsUser user)
        {
            try
            {
                _userRepository.DeleteAsync(user);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Task<IEnumerable<CmsUser>> GetAllUsersAsync()
        {
            var users = _userRepository.GetAllUsersAsync();

            return users;
        }

        public Task<CmsUser> GetLoginUserAsync(string userName, string password)
        {
            var userLogin = _userRepository.GetLoginUserAsync(userName, password);

            return userLogin;
        }

        public Task<IEnumerable<string>> GetRolesForUserAsync(CmsUser user)
        {
            var userRoles = _userRepository.GetRolesForUserAsync(user);

            return userRoles;
        }

        public Task<CmsUser> GetUserByNameAsync(string username)
        {
            var user = _userRepository.GetUserByNameAsync(username);

            return user;
        }

        public string HashPassword(string password)
        {
            var hashedPassword = _userRepository.HashPassword(password);

            return hashedPassword;
        }

        public bool RemoveUserFromRoleAsync(CmsUser user, params string[] roleNames)
        {
            try
            {
                _userRepository.RemoveUserFromRoleAsync(user, roleNames);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateAsync(CmsUser user)
        {
            try
            {
                _userRepository.UpdateAsync(user);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool VerifyUserPassword(string hashedPassword, string providedPassword)
        {
            try
            {
                _userRepository.VerifyUserPassword(hashedPassword, providedPassword);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _userRepository.Dispose();
            }

            _disposed = true;
        }
    }
}
