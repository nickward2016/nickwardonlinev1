﻿namespace NickWardOnline.Services
{
	using NickWardOnline.Models.DomainModels;
	using NickWardOnline.Repositories;
	using System;
	using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;

    /// <summary>
    /// PostService class
    /// </summary>
    public class PostService : IPostService
    {
        private IPostRepository _postRepository;

        public PostService()
            :this(new PostRepository()) { }

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public bool Create(HttpPostedFileBase file, Post model)
        {
            try
            {
                _postRepository.Create(file, model);

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                _postRepository.Delete(id);

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool Edit(HttpPostedFileBase file, string id, Post updatedItem)
        {
            try
            {
                _postRepository.Edit(file, id, updatedItem);

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public int CountPublishedPosts()
        {
            var posts = _postRepository.CountPublished;

            return posts;
        }

        public Task<IEnumerable<Post>> GetAllAsync()
        {
            var posts = _postRepository.GetAllAsync();

            return posts;
        }

        public Task<IEnumerable<Post>> GetPageAsync(int pageNumber, int pageSize)
        {
            var pages = _postRepository.GetPageAsync(pageNumber, pageSize);

            return pages;
        }

        public Post Get(string id)
        {
            var posts = _postRepository.Get(id);

            return posts;
        }

        public Task<IEnumerable<Post>> GetByAuthorAsync(string authorId)
        {
            var posts = _postRepository.GetPostsByAuthorAsync(authorId);

            return posts;
        }

        public Task<IEnumerable<Post>> GetByTagAsync(string tagId)
        {
            var posts = _postRepository.GetPostsByTagAsync(tagId);

            return posts;
        }

        public Task<IEnumerable<Post>> GetPublishedPostsAsync()
        {
            var posts = _postRepository.GetPublishedPostsAsync();

            return posts;
        }

		public Task<IEnumerable<Post>> GetPostsByAuthorAsync(string authorId)
		{
			var posts = _postRepository.GetPostsByAuthorAsync(authorId);

			return posts;
		}
	}
}
