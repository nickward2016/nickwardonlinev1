﻿namespace NickWardOnline.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity.EntityFramework;
	using NickWardOnline.Repositories;

	/// <summary>
	/// RoleService class
	/// </summary>
	public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService()
            :this(new RoleRepository()) { }

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public bool CreateAsync(IdentityRole role)
        {
            try
            {
                _roleRepository.CreateAsync(role);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Task<IEnumerable<IdentityRole>> GetAllRolesAsync()
        {
            var roles = _roleRepository.GetAllRolesAsync();

            return roles;
        }

        public Task<IdentityRole> GetRoleByNameAsync(string name)
        {
            var roles = _roleRepository.GetRoleByNameAsync(name);

            return roles;
        }
    }
}
