﻿namespace NickWardOnline.Repositories
{
	using NickWardOnline.Models.Context;
	using Serilog;
	using System;
    using System.Collections.Generic;
	using System.Linq;

    /// <summary>
    /// TagRepository class
    /// </summary>
    public class TagRepository : ITagRepository
    {
        private CmsContext _db; 

        public TagRepository()
        {
			Log.Logger = new LoggerConfiguration()
				.ReadFrom.AppSettings()
				.CreateLogger();

            _db = new CmsContext();
        }

        public string Get(string tag)
        {
            var posts = _db.Posts.Where(post => post.CombinedTags.Contains(tag)).ToList();

            posts = posts.Where(post =>
                        post.Tags.Contains(tag, StringComparer.CurrentCultureIgnoreCase))
                        .ToList();

            if (!posts.Any())
            {
                throw new KeyNotFoundException("The tag " + tag + " does not exist.");
            }

            return tag.ToLower();
        }

        public IEnumerable<string> GetAll()
        {
            var tagsCollection = _db.Posts.Select(p => p.CombinedTags).ToList();
            return string.Join(",", tagsCollection).Split(',').Distinct();
        }

        public void Delete(string tag)
        {
            var posts = _db.Posts.Where(post => post.CombinedTags.Contains(tag)).ToList();

            posts = posts.Where(post =>
                post.Tags.Contains(tag, StringComparer.CurrentCultureIgnoreCase))
                .ToList();

            if (!posts.Any())
            {
                throw new KeyNotFoundException("The tag " + tag + " does not exist.");
            }

            foreach (var post in posts)
            {
                post.Tags.Remove(tag);
            }

			try
			{
				_db.SaveChanges();
			}
			catch(Exception ex)
			{
				Log.Information("An error has occurred", ex);
			}
        }

        public void Edit(string existingTag, string newTag)
        {
            var posts = _db.Posts.Where(post => post.CombinedTags.Contains(existingTag)).ToList();

            posts = posts.Where(post =>
                        post.Tags.Contains(existingTag, StringComparer.CurrentCultureIgnoreCase))
                        .ToList();

            if (!posts.Any())
            {
                throw new KeyNotFoundException("The tag " + existingTag + " does not exist.");
            }

            foreach (var post in posts)
            {
                post.Tags.Remove(existingTag);
                post.Tags.Add(newTag);
            }

			try
			{
				_db.SaveChanges();
			}
			catch(Exception ex)
			{
				Log.Information("An error has occurred", ex);
			}
        }
    }
}
