﻿namespace NickWardOnline.Repositories
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;
	using System.Data.Entity;
	using System.Web;
	using NickWardOnline.Models.DomainModels;
	using NickWardOnline.Models.Context;
	using Serilog;

	/// <summary>
	/// Post Repository class
	/// </summary>
	public class PostRepository : IPostRepository
	{
		private CmsContext _db;

		public PostRepository()
		{
			Log.Logger = new LoggerConfiguration()
				.ReadFrom.AppSettings()
				.CreateLogger();

			_db = new CmsContext();
		}

		public int CountPublished
		{
			get
			{
				return _db.Posts.Count(p => p.Published < DateTime.Now);
			}
		}

		public Post Get(string id)
		{
			var result = _db.Posts.Include("Author")
				.SingleOrDefault(post => post.Id == id);

			return result;
		}

		public async Task<IEnumerable<Post>> GetAllAsync()
		{
			return await _db.Posts.Include("Author")
				.OrderByDescending(post => post.Created).ToArrayAsync();
		}

		public async Task<IEnumerable<Post>> GetPostsByAuthorAsync(string authorId)
		{
			return await _db.Posts.Include("Author")
				.Where(p => p.AuthorId == authorId)
				.OrderByDescending(post => post.Created).ToArrayAsync();
		}

		public void Create(HttpPostedFileBase file, Post model)
		{
			var post = _db.Posts.SingleOrDefault(p => p.Id == model.Id);

			if (post != null)
			{
				throw new ArgumentException("A post with the id of " + model.Id + " already exists.");
			}

			try
			{
				_db.Posts.Add(model);
				_db.SaveChanges();
			}
			catch (Exception ex)
			{
				Log.Information("An error has occurred", ex);
			}
		}

		public void Edit(HttpPostedFileBase file, string id, Post updatedItem)
		{
			var post = _db.Posts.SingleOrDefault(p => p.Id == id);

			if (post == null)
			{
				throw new KeyNotFoundException("A post with the id of "
					+ id + " does not exist in the data store.");
			}

			post.Id = updatedItem.Id;
			post.Title = updatedItem.Title;
			post.Content = updatedItem.Content;
			post.Published = updatedItem.Published;
			post.Tags = updatedItem.Tags;

			try
			{
				_db.SaveChanges();
			}
			catch (Exception ex)
			{
				Log.Information("An error has occurred", ex);
			}
		}

		public void Delete(string id)
		{
			var post = _db.Posts.SingleOrDefault(p => p.Id == id);

			if (post == null)
			{
				throw new KeyNotFoundException("The post with the id of " + id + " does not exist.");
			}

			try
			{
				_db.Posts.Remove(post);
				_db.SaveChanges();
			}
			catch (Exception ex)
			{
				Log.Information("An error has occurred", ex);
			}
		}

		public async Task<IEnumerable<Post>> GetPostsByTagAsync(string tagId)
		{
			var posts = await _db.Posts
				.Include("Author")
				.Where(p => p.CombinedTags.Contains(tagId))
				.ToListAsync();

			return posts.Where(p =>
				p.Tags.Contains(tagId, StringComparer.CurrentCultureIgnoreCase))
				.ToList();
		}

		public async Task<IEnumerable<Post>> GetPublishedPostsAsync()
		{
			return await _db.Posts
				.Include("Author")
				.Where(p => p.Published < DateTime.Now)
				.OrderByDescending(p => p.Published)
				.ToArrayAsync();
		}

		public async Task<IEnumerable<Post>> GetPageAsync(int pageNumber, int pageSize)
		{
			var skip = (pageNumber - 1) * pageSize;

			return await _db.Posts.Where(p => p.Published < DateTime.Now)
				.Include("Author")
				.OrderByDescending(p => p.Published)
				.Skip(skip)
				.Take(pageSize)
				.ToArrayAsync();
		}
	}
}
