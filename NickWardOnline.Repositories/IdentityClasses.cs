﻿namespace NickWardOnline.IdentityClasses
{
    using System;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models.CustomModels;
	using NickWardOnline.Models.Context;

	/// <summary>
	/// CmsUserStore Class
	/// </summary>
	public class CmsUserStore : UserStore<CmsUser>
    {
        public CmsUserStore()
            : this(new CmsContext())
        { }

        public CmsUserStore(CmsContext context)
            : base(context)
        { }
    }

    /// <summary>
    /// CmsUserManager Class
    /// </summary>
    public class CmsUserManager : UserManager<CmsUser>
    {
        public CmsUserManager()
            : this(new CmsUserStore())
        { }

        public CmsUserManager(UserStore<CmsUser> userStore)
            : base(userStore)
        { }
    }
}
