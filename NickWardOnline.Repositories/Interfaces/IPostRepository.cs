﻿namespace NickWardOnline.Repositories
{
	using System.Collections.Generic;
    using System.Threading.Tasks;
	using System.Web;

	/// <summary>
	/// IPostRepository interface
	/// </summary>
	public interface IPostRepository
    {
        int CountPublished { get; }
		Models.DomainModels.Post Get(string id);
        void Edit(HttpPostedFileBase file, string id, Models.DomainModels.Post updatedItem);
        void Create(HttpPostedFileBase file, Models.DomainModels.Post model);
        void Delete(string id);
        Task<IEnumerable<Models.DomainModels.Post>> GetAllAsync();
        Task<IEnumerable<Models.DomainModels.Post>> GetPostsByAuthorAsync(string authorId);
        Task<IEnumerable<Models.DomainModels.Post>> GetPostsByTagAsync(string tagId);
        Task<IEnumerable<Models.DomainModels.Post>> GetPublishedPostsAsync();
        Task<IEnumerable<Models.DomainModels.Post>> GetPageAsync(int pageNumber, int pageSize);
    }
}
