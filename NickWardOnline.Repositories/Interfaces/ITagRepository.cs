﻿namespace NickWardOnline.Repositories
{
    using System.Collections.Generic;

    /// <summary>
    /// ITagRepository interface
    /// </summary>
    public interface ITagRepository
    {
        string Get(string tag);
        IEnumerable<string> GetAll();
        void Edit(string existingTag, string newTag);
        void Delete(string tag);
    }
}
