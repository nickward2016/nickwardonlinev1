﻿using System.Web.Optimization;

namespace NickWardOnline.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/assets/js/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-validate").Include(
                        "~/assets/js/jquery.unobtrusive-ajax.min.js",
                        "~/assets/js/jquery.validate.js",
                        "~/assets/js/jquery.validate.unobtrusive.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/assets/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/assets/plugins/scripts.js",
                        "~/assets/plugins/slider.elastic/js/jquery.eislideshow.js",
                        "~/assets/js/view/demo.elastic_slider.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/admin").Include(
                        "~/Scripts/Application/admin/admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/post").Include(
                        "~/Scripts/Application/post/post.js"));


            bundles.Add(new StyleBundle("~/bundles/jquery-ui-css").Include(
                        "~/Content/base/themes/base/smoothness/jquery.ui.all.css"));

            bundles.Add(new StyleBundle("~/core/css").Include(
                      "~/assets/plugins/slider.elastic/css/style.css",
                      "~/assets/css/essentials.css",
                      "~/assets/css/layout.css",
                      "~/assets/css/header-0.css",
                      "~/assets/css/colour-scheme/blue.css"));

            bundles.Add(new StyleBundle("~/core/css/admin").Include(
                      "~/assets/css/Site.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
