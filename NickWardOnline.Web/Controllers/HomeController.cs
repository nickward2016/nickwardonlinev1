﻿namespace NickWardOnline.Web.Controllers
{
    using AutoMapper;
    using NickWardOnline.Web.Models.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Configuration;
	using NickWardOnline.Models.DomainModels;
	using NickWardOnline.Services;
	using NickWardOnline.Repositories;

	public class HomeController : Controller
    {
        private readonly IPostService _postService;
		private readonly IPostRepository _postRepository;
        private readonly int _pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);

        public HomeController() : this(new PostService(), new PostRepository()) { }

        public HomeController(IPostService postService, IPostRepository postRepository)
        {
            _postService = postService;
			_postRepository = postRepository;
        }

        [Route("")]
        public async Task<ActionResult> Index()
        {
            var posts = await _postService.GetPageAsync(1, _pageSize);

            var postsMapped = Mapper.Map<IEnumerable<Post>, IEnumerable<vmPost>>(posts);

            ViewBag.PreviousPage = 0;
			ViewBag.NextPage = (Decimal.Divide(_postRepository.CountPublished, _pageSize) > 1) ? 2 : -1;

            return View(postsMapped);
        }

        [Route("page/{page:int}")]
        public async Task<ActionResult> Page(int page = 1)
        {
            if (page < 2)
            {
                return RedirectToAction("index");
            }

            var posts = await _postService.GetPageAsync(page, _pageSize);

            var postsMapped = Mapper.Map<IEnumerable<Post>, IEnumerable<vmPost>>(posts);

            ViewBag.PreviousPage = page - 1;
            ViewBag.NextPage = (Decimal.Divide(_postRepository.CountPublished, _pageSize) > page) ? page + 1 : -1;

            return View("index", postsMapped);
        }

        [Route("posts/{postId}")]
        public async Task<ActionResult> Post(string postId)
        {
            var post = _postService.Get(postId);

            var postsMapped = Mapper.Map<Post, vmPost>(post);

            if (post == null)
            {
                return HttpNotFound();
            }

            return View(postsMapped);
        }

        [Route("tags/{tagId}")]
        public async Task<ActionResult> Tag(string tagId)
        {
            var posts = await _postRepository.GetPostsByTagAsync(tagId);

            var postsMapped = Mapper.Map<IEnumerable<Post>, IEnumerable<vmPost>>(posts);

            if (!posts.Any())
            {
                return HttpNotFound();
            }

            ViewBag.Tag = tagId;

            return View(postsMapped);
        }

        public async Task<PartialViewResult> MainMenu()
        {
            var menu = new List<vmAdminMenuItem>();

            menu.Add(new vmAdminMenuItem
            {
                Text = "home",
                Action = "index",
                RouteInfo = new { controller = "home" }
            });

            return PartialView(menu);
        }
    }
}