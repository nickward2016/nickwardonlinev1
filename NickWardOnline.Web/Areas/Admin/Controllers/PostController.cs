﻿namespace NickWardOnline.Web.Areas.Admin.Controllers
{   using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Models.ViewModels;
    using AutoMapper;
	using NickWardOnline.Models.DomainModels;
	using System.Threading.Tasks;
	using NickWardOnline.Services;
	using NickWardOnline.Models.CustomModels;
	using NickWardOnline.Web.Helpers;
	using System.Linq;
	using System.IO;
	using System.Web;
	using NickWardOnline.ServiceClient.Client;
	using System.Configuration;

	[RouteArea("Admin")]
	[RoutePrefix("post")]
	[ValidateInput(true)]
	[Authorize]
	public class PostController : Controller
	{
		private IPostService _postService;
		private readonly IUserService _userService;

		private ApiClient _client;

		public PostController() : this(new PostService(), new UserService()) { }

		public PostController(IPostService postService, IUserService userService)
		{
			_postService = postService;
			_userService = userService;
			_client = new ApiClient(new Uri(ConfigurationManager.AppSettings["api-url"]));
		}

		[Route("")]
		public async Task<ActionResult> Index()
		{
			var posts = _client.GetAllPosts();

			var postsMapped = Mapper.Map<IEnumerable<Post>, IEnumerable<vmPost>>(posts);

			return View(postsMapped);
		}

		[HttpGet]
		[Route("create")]
		public ActionResult Create()
		{
			return View(new vmPost());
		}

		[HttpPost]
		[Route("create")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(vmPost model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await GetLoggedInUser();

			if (string.IsNullOrWhiteSpace(model.Id))
			{
				model.Id = model.Title;
			}

			model.Id = model.Id.MakeUrlFriendly();
			model.Tags = model.Tags.Select(tag => tag.MakeUrlFriendly()).ToList();
			model.Created = DateTime.UtcNow;
			model.AuthorId = user.Id;

			var file = Request.Files["ImageData"];

			if (file.ContentLength > 0)
			{
				string relativePath = @"~/Images/" + Path.GetFileName(file.FileName);
				string physicalPath = Server.MapPath(relativePath);
				file.SaveAs(physicalPath);
				model.ImagePath = relativePath;
			}

			var postsMapped = Mapper.Map<vmPost, Post>(model);

			try
			{
				_postService.Create(file, postsMapped);

				return RedirectToAction("index");
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("key", ex);
				return View(model);
			}
		}

		[HttpGet]
		[Route("edit/{postId}")]
		public async Task<ActionResult> Edit(string postId)
		{
			var post = _postService.Get(postId);

			var mappedPosts = Mapper.Map<Post, vmPost>(post);

			if (mappedPosts == null)
			{
				return HttpNotFound();
			}

			if (User.IsInRole("author"))
			{
				var user = await GetLoggedInUser();

				if (mappedPosts.AuthorId != user.Id)
				{
					return new HttpUnauthorizedResult();
				}
			}

			return View(mappedPosts);
		}

		[HttpPost]
		[Route("edit/{postId}")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(HttpPostedFileBase file, string postId, vmPost model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			if (User.IsInRole("author"))
			{
				var user = await GetLoggedInUser();
				var post = _postService.Get(postId);

				try
				{
					if (post.AuthorId != user.Id)
					{
						return new HttpUnauthorizedResult();
					}
				}
				catch(Exception ex)
				{

				}
			}

			if (string.IsNullOrWhiteSpace(model.Id))
			{
				model.Id = model.Title;
			}

			model.Id = model.Id.MakeUrlFriendly();
			model.Tags = model.Tags.Select(tag => tag.MakeUrlFriendly()).ToList();

			var mappedPosts = Mapper.Map<vmPost, Post>(model);

			try
			{
				_postService.Edit(file, postId, mappedPosts);

				return RedirectToAction("index");
			}
			catch (KeyNotFoundException e)
			{
				return HttpNotFound();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, ex.Message);
				return View(model);
			}
		}

		[HttpGet]
		[Route("delete/{postId}")]
		[Authorize(Roles = "admin, editor")]
		public ActionResult Delete(string postId, string foo)
		{
			var post = _postService.Get(postId);

			var mappedPosts = Mapper.Map<Post, vmPost>(post);

			if (mappedPosts == null)
			{
				return HttpNotFound();
			}

			return View(mappedPosts);
		}

		[HttpPost]
		[Route("delete/{postId}")]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "admin, editor")]
		public ActionResult Delete(string postId)
		{
			try
			{
				_postService.Delete(postId);

				return RedirectToAction("index");
			}
			catch (KeyNotFoundException ex)
			{
				return HttpNotFound();
			}
		}

		private async Task<CmsUser> GetLoggedInUser()
		{
			return await _userService.GetUserByNameAsync(User.Identity.Name);
		}

		private bool _isDisposed;
		protected override void Dispose(bool disposing)
		{

			if (!_isDisposed)
			{
				_userService.Dispose();
			}
			_isDisposed = true;

			base.Dispose(disposing);
		}
	}
}