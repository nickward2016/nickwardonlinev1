﻿namespace NickWardOnline.Web.Areas.Admin.Controllers
{
	using NickWardOnline.Repositories;
	using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    [RouteArea("admin")]
    [RoutePrefix("tag")]
    [Authorize]
    public class TagController : Controller
    {
        private readonly ITagRepository _tagRepository;

        public TagController() : this(new TagRepository()) { }

        public TagController(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }

        [Route("")]
        public ActionResult Index()
        {
            var tags = _tagRepository.GetAll();

            return View(tags);
        }
    
        [HttpGet]
        [Route("edit/{tag}")]
        [Authorize(Roles = "admin, editor")]
        public ActionResult Edit(string tag)
        {
            try
            {
                var model = _tagRepository.Get(tag);
                return View(model: model);
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("edit/{tag}")]
        [Authorize(Roles = "admin, editor")]
        public ActionResult Edit(string tag, string newTag)
        {
            var tags = _tagRepository.GetAll();

            if (!tags.Contains(tag))
            {
                return HttpNotFound();
            }

            if (tags.Contains(newTag))
            {
                return RedirectToAction("index");
            }

            if (string.IsNullOrWhiteSpace(newTag))
            {
                ModelState.AddModelError("key", "New tag value cannot be empty.");

                return View(model: tag);
            }

            _tagRepository.Edit(tag, newTag);

            return RedirectToAction("index");
        }

        [HttpGet]
        [Route("delete/{tag}")]
        [Authorize(Roles = "admin, editor")]
        public ActionResult Delete(string tag)
        {
            try
            {
                var model = _tagRepository.Get(tag);
                return View(model: model);
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("delete/{tag}")]
        [Authorize(Roles = "admin, editor")]
        public ActionResult Delete(string tag, string foo)
        {
            try
            {
                _tagRepository.Delete(tag);
                return RedirectToAction("index");
            }
            catch (KeyNotFoundException ex)
            {
                return HttpNotFound();
            }
        }
    }
}