﻿namespace NickWardOnline.Web.Areas.Admin.Controllers
{
    using System.Web.Mvc;
    using System.Threading.Tasks;
    using Models.ViewModels;
    using System.Web;
    using Microsoft.Owin.Security;
    using System.Collections.Generic;
	using NickWardOnline.Repositories;

	[RouteArea("admin")]
    [Authorize]
	[ValidateInput(true)]
	public class AdminController : Controller
    {
        private readonly IUserRepository _users;

        public AdminController() : this(new UserRepository()) { }

        public AdminController(IUserRepository users)
        {
            _users = users;
        }

        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login()
        {
            return View();
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login(vmLogin model)
        {
			if (ModelState.IsValid)
			{
				var user = await _users.GetLoginUserAsync(model.UserName, model.Password);

				if (user == null)
				{
					ModelState.AddModelError(string.Empty, "The user with the supplied credentials does not exist.");
				}

				var authManager = HttpContext.GetOwinContext().Authentication;
				var userIdentity = await _users.CreateIdentityAsync(user);

				authManager.SignIn(new AuthenticationProperties() { IsPersistent = model.RememberMe }, userIdentity);
			}
			return View();
        }

        [Route("Logout")]
        public async Task<ActionResult> Logout()
        {
            var authManager = HttpContext.GetOwinContext().Authentication;

            authManager.SignOut();

            return RedirectToAction("index");
        }

        [AllowAnonymous]
        public async Task<PartialViewResult> AdminMenu()
        {
            var items = new List<vmAdminMenuItem>();

            if (User.Identity.IsAuthenticated)
            {
                items.Add(new vmAdminMenuItem
                {
                    Text = "Admin Home",
                    Action = "index",
                    RouteInfo = new { controller = "admin", area = "admin" }
                });

                if (User.IsInRole("admin"))
                {
                    items.Add(new vmAdminMenuItem
                    {
                        Text = "Users",
                        Action = "index",
                        RouteInfo = new { controller = "user", area = "admin" }
                    });
                }
                else
                {
                    items.Add(new vmAdminMenuItem
                    {
                        Text = "Profile",
                        Action = "edit",
                        RouteInfo = new { controller = "user", area = "admin", username = User.Identity.Name }
                    });
                }

                if (!User.IsInRole("author"))
                {
                    items.Add(new vmAdminMenuItem
                    {
                        Text = "Tags",
                        Action = "index",
                        RouteInfo = new { controller = "tag", area = "admin" }
                    });
                }

                items.Add(new vmAdminMenuItem
                {
                    Text = "Posts",
                    Action = "index",
                    RouteInfo = new { controller = "post", area = "admin" }
                });
            }

            return PartialView(items);
        }

        [AllowAnonymous]
        public async Task<PartialViewResult> AuthenticationLink()
        {
            var item = new vmAdminMenuItem
            {
                RouteInfo = new { controller = "admin", area = "admin" }
            };

            if (User.Identity.IsAuthenticated)
            {
                item.Text = "Logout";
                item.Action = "logout";
            }
            else
            {
                item.Text = "Login";
                item.Action = "login";
            }

            return PartialView("_menuLink", item);
        }

        private bool _isDisposed;

        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                _users.Dispose();
            }

            _isDisposed = true;

            base.Dispose(disposing);
        }
    }
}