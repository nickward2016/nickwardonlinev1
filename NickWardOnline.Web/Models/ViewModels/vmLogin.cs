﻿namespace NickWardOnline.Web.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class vmLogin
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "* A valid username is required.")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 8, ErrorMessage = "* A valid password is required.")]
        public string Password { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe { get; set; }
    }
}