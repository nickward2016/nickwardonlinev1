﻿namespace NickWardOnline.Web.Models.ViewModels
{
	using NickWardOnline.Models.CustomModels;
	using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web.Mvc;

    public class vmPost
    {
		[Display(Name = "Slug")]
        public string Id { get; set; }

        [Display(Name = "Title")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Content")]
        [AllowHtml]
        public string Content { get; set; }

        [Display(Name = "Upload Image")]
        public string ImagePath { get; set; }

        [Display(Name = "Date Created")]
        public DateTime Created { get; set; }

		[Display(Name = "Date Published")]
        public DateTime? Published { get; set; }

		public IList<string> Tags { get; set; } = new List<string>();

		public string CombinedTags
        {
            get { return string.Join(",", Tags); }
            set
            {
                Tags = value.Split(',').Select(s => s.Trim()).ToList();
            }
        }

        public string AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public virtual CmsUser Author { get; set; }
    }
}