﻿namespace NickWardOnline.Web
{
    using AutomapperProfiles;
	using NickWardOnline.Web.Models.ModelBinders;
	using NickWardOnline.Models.DomainModels;
	using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.Add(typeof(Post), new PostModelBinder());
			BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutomapperConfiguration.ConfigureMappings();
		}
    }
}
