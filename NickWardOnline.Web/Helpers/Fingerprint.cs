﻿namespace NickWardOnline.Web.Helpers
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.Caching;
    using System.Web.Hosting;

    public class Fingerprint
    {
        /// <summary>
        /// Adds a fingerprint or timestamp to the URL of the static file
        /// </summary>
        /// <param name="rootRelativePath"></param>
        /// <returns></returns>
        public static string Tag(string rootRelativePath)
        {
            if (HttpRuntime.Cache[rootRelativePath] == null)
            {
                string absolute = HostingEnvironment.MapPath("~" + rootRelativePath);

                DateTime date = File.GetLastWriteTime(absolute);
                int index = rootRelativePath.LastIndexOf('/');

                string result = rootRelativePath.Insert(index, "/v-" + date.Ticks);
                HttpRuntime.Cache.Insert(rootRelativePath, result, new CacheDependency(absolute));
            }

            return HttpRuntime.Cache[rootRelativePath] as string;
        }
    }
}