﻿namespace NickWardOnline.Web.Helpers
{
    using Models.ViewModels;
    using System.Collections.Generic;

    /// <summary>
    /// Menu Helper
    /// </summary>
    public class MenuHelper
    {
        /// <summary>
        /// Configure Menu Items
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="Action"></param>
        /// <param name="Controller"></param>
        /// <param name="Area"></param>
        /// <returns></returns>
        public IEnumerable<vmAdminMenuItem> Configure(string Text, string Action, string Controller, string Area = "default")
        {
            var items = new List<vmAdminMenuItem>();

            items.Add(new vmAdminMenuItem
            {
                Text = Text,
                Action = Action,
                RouteInfo = new { controller = Controller, area = Area }
            });

            return items;
        }
    }
}