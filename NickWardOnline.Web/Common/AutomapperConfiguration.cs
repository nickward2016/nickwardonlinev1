﻿namespace NickWardOnline.Web.AutomapperProfiles
{
    using AutoMapper;
    using Models.ViewModels;
	using NickWardOnline.Models.DomainModels;
	using System.Collections.Generic;

	public class AutomapperConfiguration : Profile
    {
        public static void ConfigureMappings()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Post, vmPost>().ReverseMap();
            });
        }
    }
}