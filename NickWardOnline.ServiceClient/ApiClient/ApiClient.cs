﻿namespace NickWardOnline.ServiceClient.Client
{
	using NickWardOnline.Models.DomainModels;
	using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;

    public class ApiClient
    {
        private Uri baseAddress;

        public ApiClient(Uri baseAddress)
        {
            this.baseAddress = baseAddress;
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return Get<IEnumerable<Post>>("api/posts");
        }

        public Post CreatePosts()
        {
            return Create<Post>("api/posts");
        }

        public Post EditPosts()
        {
            return Edit<Post>("api/post/edit");
        }

        public Post DeletePosts()
        {
            return Delete<Post>("api/post/delete");
        }

        private T Get<T>(string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.GetAsync(query).Result;
                response.EnsureSuccessStatusCode();
                return response.Content.ReadAsAsync<T>().Result;
            }
        }

        private T Create<T>(string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.PostAsJsonAsync(query, new Post()).Result;
                response.EnsureSuccessStatusCode();
                return response.Content.ReadAsAsync<T>().Result;
            }
        }

        private T Edit<T>(string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.PutAsJsonAsync(query, new Post()).Result;
                response.EnsureSuccessStatusCode();
                return response.Content.ReadAsAsync<T>().Result;
            }
        }

        private T Delete<T>(string query)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = baseAddress;
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = httpClient.DeleteAsync(query).Result;
                response.EnsureSuccessStatusCode();
                return response.Content.ReadAsAsync<T>().Result;
            }
        }
    }
}
