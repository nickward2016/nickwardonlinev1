﻿namespace NickWardOnline.API.Helpers
{
	using System.Text.RegularExpressions;

	public static class StringHelper
	{
		public static string MakeUrlFriendly(this string value)
		{
			value = value.ToLowerInvariant().Replace(" ", "-");
			value = Regex.Replace(value, @"[^0-9a-z-]", string.Empty);

			return value;
		}
	}
}
