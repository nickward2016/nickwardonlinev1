﻿namespace NickWardOnline.API.Controllers
{
	using NickWardOnline.API.Helpers;
	using NickWardOnline.Models.CustomModels;
	using NickWardOnline.Models.DomainModels;
	using NickWardOnline.Services;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Net;
	using System.Threading.Tasks;
	using System.Web;
	using System.Web.Http;

	[RoutePrefix("api/posts")]
	public class PostsController : ApiController
	{
		private readonly IPostService _postService;
		private readonly IUserService _userService;

		public PostsController()
			: this(new PostService(), new UserService()) { }

		public PostsController(IPostService postService, IUserService userService)
		{
			_postService = postService;
			_userService = userService;
		}

		[HttpGet]
		public async Task<IHttpActionResult> GetAll()
		{
			try
			{
				if (!User.IsInRole("author"))
				{
					var posts = await _postService.GetAllAsync();

					return Ok(posts);
				}

				var user = await GetLoggedInUser();

				var getPostsByAuthor = await _postService.GetByAuthorAsync(user.Id);

				return Ok(getPostsByAuthor);
			}
			catch (Exception ex)
			{
				return NotFound();
			}
		}

		[HttpPost]
		public async Task<IHttpActionResult> Create(Post model)
		{
			if (!ModelState.IsValid)
			{
				return Ok(model);
			}

			var user = await GetLoggedInUser();

			if (string.IsNullOrWhiteSpace(model.Id))
			{
				model.Id = model.Title;
			}

			model.Id = model.Id.MakeUrlFriendly();
			model.Tags = model.Tags.Select(tag => tag.MakeUrlFriendly()).ToList();
			model.Created = DateTime.UtcNow;
			model.AuthorId = user.Id;

			var file = HttpContext.Current.Request.Files["ImageData"];

			if (file.ContentLength > 0)
			{
				string relativePath = @"~/Images/" + Path.GetFileName(file.FileName);
				string physicalPath = HttpContext.Current.Server.MapPath(relativePath);
				file.SaveAs(physicalPath);
				model.ImagePath = relativePath;
			}

			try
			{
				//_postService.Create(file, model);

				return Ok(model);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("key", ex);
				return Ok(model);
			}
		}

		[HttpGet]
		[Route("edit/{postId}")]
		public async Task<IHttpActionResult> Edit([FromBody] string postId)
		{
			var post = _postService.Get(postId);

			if (post == null)
			{
				return NotFound();
			}

			if (User.IsInRole("author"))
			{
				var user = await GetLoggedInUser();

				if (post.AuthorId != user.Id)
				{
					throw new HttpResponseException(HttpStatusCode.Unauthorized);
				}
			}

			return Ok(post);
		}

		[HttpPost]
		[Route("edit/{postId}")]
		public async Task<IHttpActionResult> Edit([FromBody] string postId, [FromBody] Post model)
		{
			if (!ModelState.IsValid)
			{
				return Ok(model);
			}

			if (User.IsInRole("author"))
			{
				var user = await GetLoggedInUser();
				var post = _postService.Get(postId);

				try
				{
					if (post.AuthorId != user.Id)
					{
						throw new HttpResponseException(HttpStatusCode.Unauthorized);
					}
				}
				catch (Exception ex)
				{
				}
			}

			if (string.IsNullOrWhiteSpace(model.Id))
			{
				model.Id = model.Title;
			}

			model.Id = model.Id.MakeUrlFriendly();
			model.Tags = model.Tags.Select(tag => tag.MakeUrlFriendly()).ToList();

			var file = HttpContext.Current.Request.Files["ImageData"];

			try
			{
				//_postService.Edit(file, postId, model);

				return Ok(model);
			}
			catch (KeyNotFoundException ex)
			{
				return NotFound();
			}
			catch (Exception ex)
			{
				ModelState.AddModelError(string.Empty, ex.Message);
				return Ok(model);
			}
		}

		[HttpGet]
		[Route("delete/{postId}")]
		[Authorize(Roles = "admin, editor")]
		public IHttpActionResult Delete([FromBody] string postId, [FromBody] string foo)
		{
			var post = _postService.Get(postId);

			if (post == null)
			{
				return NotFound();
			}

			return Ok(post);
		}

		[HttpDelete]
		[Route("delete/{postId}")]
		[Authorize(Roles = "admin, editor")]
		public IHttpActionResult Delete([FromBody] string postId)
		{
			try
			{
				_postService.Delete(postId);

				return Ok();
			}
			catch (KeyNotFoundException ex)
			{
				return NotFound();
			}
		}

		private async Task<CmsUser> GetLoggedInUser()
		{
			return await _userService.GetUserByNameAsync(User.Identity.Name);
		}

		private bool _isDisposed;
		protected override void Dispose(bool disposing)
		{
			if (!_isDisposed)
			{
				_userService.Dispose();
			}
			_isDisposed = true;

			base.Dispose(disposing);
		}
	}
}
